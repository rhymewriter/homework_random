import com.java.main.RandomNumber;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        RandomNumber randomizer = new RandomNumber();

        System.out.println("\n" + "Вывести на консоль случайное число");
        System.out.println(randomizer.getNumberOne());

        System.out.println("\n" + "Вывести на консоль 10 случайных чисел");
        System.out.println(Arrays.toString(randomizer.getTenNumbers()));

        System.out.println("\n" + "Вывести на консоль 10 случайных чисел, каждое в диапазоне от 0 до 10");
        System.out.println(Arrays.toString(randomizer.getTenNumbersFrom0To10()));

        System.out.println("\n" + "Вывести на консоль 10 случайных чисел, каждое в диапазоне от 20 до 50");
        System.out.println(Arrays.toString(randomizer.getTenNumbersFrom20To50()));

        System.out.println("\n" + "Вывести на консоль 10 случайных чисел, каждое в диапазоне от -10 до 10");
        System.out.println(Arrays.toString(randomizer.getTenNumbersFromMinus10ToPlus10()));

        System.out.println("\n" + "Вывести на консоль случайное количество (в диапазоне от 3 до 15) случайных чисел, каждое в диапазоне от -10 до 35.");
        System.out.println(Arrays.toString(randomizer.getVariableNumbersFromMinus10To35()));

        System.out.println("\n" + "Используя рандом, заполнить массив из 20 элементов неповторяющимися числами");
        System.out.println(Arrays.toString(randomizer.getRandomArray()));

    }
}
