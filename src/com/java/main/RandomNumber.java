package com.java.main;

import java.util.Random;

public class RandomNumber {
    Random random = new Random();

    public int getNumberOne() {
        return random.nextInt();
    }

    public int[] getTenNumbers() {
        int n = 0;
        int[] randNumbers = new int[10];

        while (n < 10) {
            randNumbers[n] = random.nextInt();
            n++;
        }

        return randNumbers;
    }

    public int[] getTenNumbersFrom0To10() {
        int n = 0;
        int[] randNumbers = new int[10];

        while (n < 10) {
            randNumbers[n] = random.nextInt(11);
            n++;
        }

        return randNumbers;
    }

    public int[] getTenNumbersFrom20To50() {
        int n = 0;
        int[] randNumbers = new int[10];

        while (n < 10) {
            randNumbers[n] = random.nextInt(31) + 20;
            n++;
        }

        return randNumbers;
    }

    public int[] getTenNumbersFromMinus10ToPlus10() {
        int n = 0;
        int[] randNumbers = new int[10];

        while (n < 10) {
            randNumbers[n] = random.nextInt(21) - 10;
            n++;
        }

        return randNumbers;
    }

    public int[] getVariableNumbersFromMinus10To35() {
        int n = 0;
        int range = random.nextInt(13) + 3;
        int[] randNumbers = new int[range];

        while (n < range) {
            randNumbers[n] = random.nextInt(46) - 10;
            n++;
        }

        return randNumbers;
    }

    public int[] getRandomArray() {
        int range = 10;
        int[] newRandNumbers = new int[range];
        int randomNumber = random.nextInt(11);
            for (int i = 0; i < newRandNumbers.length; i++) {
                if (getCheck(randomNumber, newRandNumbers)) {
                    newRandNumbers[i] = randomNumber;
                    randomNumber = random.nextInt(11);
                } else {
                    randomNumber = random.nextInt(11);
                    i--;
                }
            } return newRandNumbers;
        }

    boolean getCheck (int randomNumber, int[] newRandNumbers) {
        for (int i: newRandNumbers)
        {
            if( randomNumber == i ) {
                return false;
            }
        } return true;

    }

    }
